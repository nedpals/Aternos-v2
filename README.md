# Aternos-v2
A project to redesign Aternos.org to be fast, responsive, and mobile-friendly.

# Demo pls...
Here it is: http://nedpals.github.io/Aternos-v2


**(UPDATE: The site got some serious problems after I published the demo. So I decided to put it here in GitHub to be easier to manage and more stable.)**

# What is Aternos?
**Aternos** is the *world's largest free Minecraft server* with over **760,000+ users** and over **600,000+ servers** created.

# Reasons to improve it?
A few growing users of Aternos are starting their Minecraft servers using their smartphone and tablets. With this project, it is significantly improved the accessibility of Aternos.org to the mobile.

# More reasons?
Another reason is to improve significantly not just the website's load time but the performance of it. Also, it gives more of a user-friendly and clean look instead of a *&#42;cough&#42;* dizzy design.

# Components
Unlike the same Aternos look you see in the website right now, Aternos v2 upgrades the components to the next level:

**Twitter Bootstrap v2.3.2 -> v3.3.2** (***mobile-friendly and responsive one***)

(Thanks to *BootstrapCDN* by MaxCDN, it drastically reduces load times.)


**Jasny Bootstrap v3.1.3**
*Useless at this time, but its a extended version of Bootstrap that includes Off-canvas and beautiful file upload that can be used the future as an alternative to the few components the Bootstrap has.*

(Uses ***CloudFlare's CDN***)


Latest **jQuery** - *NOTHING AT ALL!*


**Font Awesome v4.3**
*Still waiting the v5, but it has more icons that can be used than the old one.*

(I also use the ***BootstrapCDN*** one to reduce loading times.)


**Glyphicons**
*May excluded in the next release.*

**Noty** - *No upgrades.*

**Flag Icons CSS** - *Recently added. Important in language selection*

# Component Proposals
The components I wish I would like to put it here but sadly because of too many requests it may slow the performance and loading of the site.

 - [x] **Flag Icon CSS** (Added the components list)

 - [ ] **Bootstrap Social**

 (Flat UI is now rejected. Realized that its too 'simplistic' to implement it on a site.)

# A note to all the people at Aternos
Hi, if you're reading this then you may surprised that I made one for the design for the Aternos site but if you like it then go ahead put it into the site. It makes me also happy that I contribute into the site for improving the design, performance, and the speed of it. =D

# Additional Notes
If you have seen any bugs around the project I make, feel free to send it to the issues and I will see if I have the time to see your reports.

If you would like to improve something in here, fork this project and once you are done make a pull request in here. Ok?

# Brofist
Lol no fist. *punch-the-monitor-fist * ~pewds-fan
